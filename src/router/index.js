import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

const routes = [
  {
    path: '/',
    redirect: '/home'
  },
  {
    path: '/home',
    name: 'home',
    components: {
      content: () => import('@/components/Home'),
      footer: () => import('@/components/Footer')
    }
  },
  {
    path: '/kind',
    name: 'kind',
    components: {
      header: () => import('@/components/kind/Header'),
      content: () => import('@/components/kind/Content'),
      footer: () => import('@/components/Footer')
    }
  },
  {
    path: '/cart',
    name: 'cart',
    components: {
      header: () => import('@/components/cart/Header'),
      content: () => import('@/components/cart/Content'),
      footer: () => import('@/components/Footer')
    }
  },
  {
    path: '/login',
    name: 'login',
    components: {
      content: () => import('@/components/Login')
    }
  },
  {
    path: '/user',
    name: 'user',
    components: {
      header: () => import('@/components/user/Header'),
      content: () => import('@/components/user/Content'),
      footer: () => import('@/components/Footer')
    },
    children: [
      {
        path: '',
        redirect: '/user/nologin'
      },
      {
        path: 'nologin',
        component: () => import('@/components/user/noLogin')
      },
      {
        path: 'login',
        component: () => import('@/components/user/login')
      }
    ]
  },
  {
    path: '/kind-detail/:id',
    name: 'kind-detail',
    components: {
      content: () => import('@/components/KindDetail/Index')
    }
  },
  {
    path: '/detail/:id',
    name: 'detail',
    components: {
      content: () => import('@/components/detail/Index')
    }
  },
  // {
  //   children: [
  //     {
  //       path: '',
  //       redirect: 'noLogin'
  //     },
  //     {
  //       path: 'noLogin',
  //       component: () => import('@/components/UserNoLogin')
  //     },
  //     {
  //       path: 'login',
  //       component: () => import('@/components/UserLogin')
  //     }
  //   ]
  // },
  {
    path: '/dress',
    name: 'dress',
    components: {
      content: () => import('@/components/Dress'),
      footer: () => import('@/components/Footer')
    }
  },
  {
    path: '/shoes',
    name: 'shoes',
    components: {
      content: () => import('@/components/Shoes'),
      footer: () => import('@/components/Footer')
    }
  },
  {
    path: '/makeup',
    name: 'makeup',
    components: {
      content: () => import('@/components/Makeup'),
      footer: () => import('@/components/Footer')
    }
  },
  {
    path: '/baby',
    name: 'baby',
    components: {
      content: () => import('@/components/Baby'),
      footer: () => import('@/components/Footer')
    }
  },
  {
    path: '/science',
    name: 'science',
    components: {
      content: () => import('@/components/Science'),
      footer: () => import('@/components/Footer')
    }
  },
  {
    path: '/health',
    name: 'health',
    components: {
      content: () => import('@/components/Health'),
      footer: () => import('@/components/Footer')
    }
  },
  {
    path: '/sport',
    name: 'sport',
    components: {
      content: () => import('@/components/Sport'),
      footer: () => import('@/components/Footer')
    }
  },
  {
    path: '/man',
    name: 'man',
    components: {
      content: () => import('@/components/Man'),
      footer: () => import('@/components/Footer')
    }
  },
  {
    path: '/kindcomment',
    name: 'kindcomment',
    components: {
      content: () => import('@/components/KindDetail/KindComment')
    }
  },
  {
    path: '/register',
    name: 'register',
    components: {
      content: () => import('@/components/register')
    }
  },
  {
    path: '/dresslist',
    name: 'dresslist',
    components: {
      default: () => import('@/components/Dresslist'),
      footer: () => import('@/components/Footer')
    }
  },
  {
    path: '/babylist',
    name: 'babylist',
    components: {
      default: () => import('@/components/Babylist'),
      footer: () => import('@/components/Footer')
    }
  },
  {
    path: '/healthlist',
    name: 'healthlist',
    components: {
      default: () => import('@/components/Healthlist'),
      footer: () => import('@/components/Footer')
    }
  },
  {
    path: '/makeuplist',
    name: 'makeuplist',
    components: {
      default: () => import('@/components/Makeuplist'),
      footer: () => import('@/components/Footer')
    }
  },
  {
    path: '/manlist',
    name: 'manlist',
    components: {
      default: () => import('@/components/Manlist'),
      footer: () => import('@/components/Footer')
    }
  },
  {
    path: '/sciencelist',
    name: 'sciencelist',
    components: {
      default: () => import('@/components/Sciencelist'),
      footer: () => import('@/components/Footer')
    }
  },
  {
    path: '/shoeslist',
    name: 'shoeslist',
    components: {
      default: () => import('@/components/Shoeslist'),
      footer: () => import('@/components/Footer')
    }
  },
  {
    path: '/sportlist',
    name: 'sportlist',
    components: {
      default: () => import('@/components/Sportlist'),
      footer: () => import('@/components/Footer')
    }
  },
  {
    path: '/mycollection',
    name: 'mycollection',
    components: {
      content: () => import('@/components/user/MyCollection')
    }
  },
  {
    path: '/myorder',
    name: 'myorder',
    components: {
      content: () => import('@/components/user/MyOrder')
    }
  },
  {
    path: '/myaddress',
    name: 'myaddress',
    components: {
      content: () => import('@/components/user/MyAddress')
    }
  },
  {
    path: '/order',
    name: 'order',
    components: {
      content: () => import('@/components/Order')
    }
  },
  {
    path: '/address',
    name: 'address',
    components: {
      content: () => import('@/components/Address')
    }
  },
  {
    path: '/payment',
    name: 'payment',
    components: {
      content: () => import('@/components/Payment')
    }
  },
  {
    path: '/gopayment',
    name: 'gopayment',
    components: {
      content: () => import('@/components/Gopayment')
    }
  }
]

const router = new Router({
  routes
})

export default router
